## How to use

Clone the project

```sh
git clone https://gitlab.com/Naresh_Au/demo-app.git
```

Update the "vars.auto.tfvars" file to overwrite the values in variable.tf
```sh
environmnet_name  = "demo-app"
app_host_key_pair = "naresh"

#Change below IP address to aloow SSH access from your network
ssh_access_ips    = ["23.45.223.23/32"]
```

#### Export following environment varibales( if aws creds are not configured already)

1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY
```console
export AWS_ACCESS_KEY_ID=GET_IT_FROM_AWS_ACCOUNT
export AWS_SECRET_ACCESS_KEY=GET_IT_FROM_AWS_ACCOUNT_AS_WELL
```

#### From command line
#### Create cluster
```console
cd demo-app
terraform init
terraform plan -out=tfplan.plan
terraform apply -auto-approve -input=false tfplan.plan
```
#### Destroy cluster
```console
terraform destroy -auto-approve
```
## Requirements

| Name | Version |
|------|---------|
| terraform | ~>0.12.23 |
| aws | 2.60.0 |


Source: https://registry.terraform.io/modules/terraform-aws-modules

## Providers

| Name | Version |
|------|---------|
| aws | 2.60.0 |
| template | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| app\_host\_key\_pair | SSH key pair name | `string` | n/a | yes |
| cidr | CIDR to be used in vpc | `string` | `"10.225.0.0/16"` | no |
| environmnet\_name | Environment name e.g. app-prod, app-nonprod, app-dev etc. use tfvars file to overwrite | `string` | `"demo-app-nonprod"` | no |
| private\_subnets | Private subnets | `list(string)` | <pre>[<br>  "10.225.0.0/21",<br>  "10.225.8.0/21",<br>  "10.225.16.0/21"<br>]</pre> | no |
| public\_subnets | Public subnets | `list(string)` | <pre>[<br>  "10.225.24.0/21",<br>  "10.225.32.0/21",<br>  "10.225.40.0/21"<br>]</pre> | no |
| region | AWS region | `string` | `"ap-southeast-2"` | no |
| ssh\_access\_ips | List of IPs to allow SSH access to app host | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| Application\_URL | Application load balancer url to access the application |


Terraform code:

[![Open Source Helpers](https://www.codetriage.com/guimove/terraform-aws-bastion/badges/users.svg)](https://www.codetriage.com/guimove/terraform-aws-bastion)

Terraform module which creates a secure SSH bastion on AWS.

Mainly inspired by [Securely Connect to Linux Instances Running in a Private Amazon VPC](https://aws.amazon.com/blogs/security/securely-connect-to-linux-instances-running-in-a-private-amazon-vpc/)

## Sample app

Wait for few seconds to let the app up and running.
[App docker container](https://hub.docker.com/repository/docker/bishnoink/face-dection)
