output "Application_URL" {
  description = "Application load balancer url to access the application"
  value       = "http://${aws_lb.app_lb.dns_name}"
}
