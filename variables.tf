variable "environmnet_name" {
  description = "Environment name e.g. app-prod, app-nonprod, app-dev etc. use tfvars file to overwrite"
  type        = string
  default     = "demo-app-nonprod"
}
variable "region" {
  description = "AWS region"
  type        = string
  default     = "ap-southeast-2"
}
variable "cidr" {
  description = "CIDR to be used in vpc"
  type        = string
  default     = "10.225.0.0/16"
}
variable "ssh_access_ips" {
  description = "List of IPs to allow SSH access to app host"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}
variable "public_subnets" {
  description = "Public subnets"
  type        = list(string)
  default     = ["10.225.24.0/21", "10.225.32.0/21", "10.225.40.0/21"]
}
variable "private_subnets" {
  description = "Private subnets"
  type        = list(string)
  default     = ["10.225.0.0/21", "10.225.8.0/21", "10.225.16.0/21"]
}

variable "app_host_key_pair" {
  description = "SSH key pair name"
  type        = string
}
