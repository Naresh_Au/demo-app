
#####################################
######Providers detail ##############

terraform {
  required_version = "~>0.12.23"
}

provider "aws" {
  version = "2.60.0"
  region  = var.region
}

#####################################
######Data Collections ##############

data "aws_availability_zones" "available" {
}
data "aws_caller_identity" "current" {}

data "aws_subnet" "subnets" {
  count = length([
    element(module.vpc.public_subnets, 0),
    element(module.vpc.public_subnets, 1),
    element(module.vpc.public_subnets, 2)
  ])
  id = [
    element(module.vpc.public_subnets, 0),
    element(module.vpc.public_subnets, 1),
    element(module.vpc.public_subnets, 2)
  ][count.index]
}

data "template_file" "user_data" {
  template = file("${path.module}/user_data.sh")

  vars = {
    aws_region              = var.region
    bucket_name             = "${var.environmnet_name}-demo-app-15062020"
    extra_user_data_content = ""
  }
}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]
  name_regex  = "^amzn2-ami-hvm.*-ebs"

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

#####################################
######VPC configuration ##############

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 2.6"

  name                              = "${var.environmnet_name}-${var.region}"
  cidr                              = var.cidr
  azs                               = data.aws_availability_zones.available.names
  private_subnets                   = var.private_subnets
  public_subnets                    = var.public_subnets
  reuse_nat_ips                     = false
  one_nat_gateway_per_az            = true
  enable_nat_gateway                = true
  enable_dns_hostnames              = true
  single_nat_gateway                = false
  enable_vpn_gateway                = false
  propagate_public_route_tables_vgw = true

  tags = {
    "Environment" = var.environmnet_name
  }

  public_subnet_tags = {
    "Environment" = var.environmnet_name
  }

  private_subnet_tags = {
    "Environment" = var.environmnet_name
  }
}


#####################################
######S3 bucket log collection ##############

resource "aws_s3_bucket" "app_bucket" {
  bucket = "${var.environmnet_name}-demo-app-15062020"
  acl    = "bucket-owner-full-control"

  force_destroy = true

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "log"
    enabled = true

    prefix = "logs/"

    tags = {
      rule      = "log"
      autoclean = true
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }

    expiration {
      days = 90
    }
  }

  tags = {
    "Environment" = var.environmnet_name
  }
}


#####################################
######App security groups##############

resource "aws_security_group" "app_security_group" {
  description = "Application security group"
  name        = "${var.environmnet_name}-host"
  vpc_id      = module.vpc.vpc_id
  tags = {
    "Environment" = var.environmnet_name
  }
}

resource "aws_security_group" "app_lb_security_group" {
  description = "Application LB security group"
  name        = "${var.environmnet_name}-lb-host"
  vpc_id      = module.vpc.vpc_id
  tags = {
    "Environment" = var.environmnet_name
  }
}

resource "aws_security_group_rule" "ingress_app_lb_80" {
  description = "Incoming traffic to app"
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "TCP"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.app_lb_security_group.id
}
resource "aws_security_group_rule" "ingress_app_80" {
  description = "Incoming traffic to app"
  type        = "ingress"
  from_port   = 3000
  to_port     = 3000
  protocol    = "TCP"
  source_security_group_id = aws_security_group.app_lb_security_group.id
  security_group_id = aws_security_group.app_security_group.id
}

resource "aws_security_group_rule" "ingress_app_443" {
  description = "Incoming traffic to app"
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "TCP"
  source_security_group_id = aws_security_group.app_lb_security_group.id
  security_group_id = aws_security_group.app_security_group.id
}

resource "aws_security_group_rule" "ingress_app_22" {
  description = "Incoming traffic to app"
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "TCP"
  cidr_blocks = concat(var.ssh_access_ips)
  security_group_id = aws_security_group.app_security_group.id
}

resource "aws_security_group_rule" "egress_app" {
  description = "Outgoing traffic from app"
  type        = "egress"
  from_port   = "0"
  to_port     = "65535"
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.app_security_group.id
}
resource "aws_security_group_rule" "egress_app_lb" {
  description = "Outgoing traffic from app"
  type        = "egress"
  from_port   = "0"
  to_port     = "65535"
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.app_lb_security_group.id
}


#####################################
######DIAM role for S3 bicket and SSM ##############

resource "aws_iam_role" "app_host_role" {
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Action": [
        "sts:AssumeRole"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "app_host_role_policy" {
  role = aws_iam_role.app_host_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Resource": "arn:aws:s3:::${var.environmnet_name}-demo-app-15062020/logs/*"
    },
    {
      "Effect": "Allow",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.environmnet_name}-demo-app-15062020/public-keys/*"
    },
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::${var.environmnet_name}-demo-app-15062020",
      "Condition": {
        "StringEquals": {
          "s3:prefix": "public-keys/"
        }
      }
    },
    {
            "Effect": "Allow",
            "Action": [
                "ssm:DescribeAssociation",
                "ssm:GetDeployablePatchSnapshotForInstance",
                "ssm:GetDocument",
                "ssm:DescribeDocument",
                "ssm:GetManifest",
                "ssm:GetParameter",
                "ssm:GetParameters",
                "ssm:ListAssociations",
                "ssm:ListInstanceAssociations",
                "ssm:PutInventory",
                "ssm:PutComplianceItems",
                "ssm:PutConfigurePackageResult",
                "ssm:UpdateAssociationStatus",
                "ssm:UpdateInstanceAssociationStatus",
                "ssm:UpdateInstanceInformation"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssmmessages:CreateControlChannel",
                "ssmmessages:CreateDataChannel",
                "ssmmessages:OpenControlChannel",
                "ssmmessages:OpenDataChannel"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2messages:AcknowledgeMessage",
                "ec2messages:DeleteMessage",
                "ec2messages:FailMessage",
                "ec2messages:GetEndpoint",
                "ec2messages:GetMessages",
                "ec2messages:SendReply"
            ],
            "Resource": "*"
        }
  ]
}
EOF

}

#########################################
######APP LB configuration ##############

resource "aws_lb" "app_lb" {
  internal = false
  name     = "${var.environmnet_name}-lb"

  subnets = [
    element(module.vpc.public_subnets, 0),
    element(module.vpc.public_subnets, 1),
    element(module.vpc.public_subnets, 2)
  ]

  load_balancer_type = "application"
  security_groups    = [aws_security_group.app_lb_security_group.id]
  tags = {
    "Environment" = var.environmnet_name
  }
}

resource "aws_lb_target_group" "app_lb_target_group" {
  name        = "${var.environmnet_name}-lb-target"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = module.vpc.vpc_id
  target_type = "instance"

  health_check {
    port     = "traffic-port"
    protocol = "HTTP"
  }

  tags = {
    "Environment" = var.environmnet_name
  }
}

resource "aws_lb_listener" "app_lb_listener_80" {
  default_action {
    target_group_arn = aws_lb_target_group.app_lb_target_group.arn
    type             = "forward"
  }

  load_balancer_arn = aws_lb.app_lb.arn
  port              = 80
  protocol          = "HTTP"
}

resource "aws_iam_instance_profile" "app_host_profile" {
  role = aws_iam_role.app_host_role.name
  path = "/"
}

####################################################
######Launch template for app instance##############

resource "aws_launch_template" "app_launch_template" {
  name_prefix   = var.environmnet_name
  image_id      = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"
  monitoring {
    enabled = true
  }
  network_interfaces {
    associate_public_ip_address = false
    security_groups             = [aws_security_group.app_security_group.id]
    delete_on_termination       = true
  }
  iam_instance_profile {
    name = aws_iam_instance_profile.app_host_profile.name
  }
  key_name = var.app_host_key_pair

  user_data = base64encode(data.template_file.user_data.rendered)

  tag_specifications {
    resource_type = "instance"
    tags = {
      "Environment" = var.environmnet_name
    }
  }

  tag_specifications {
    resource_type = "volume"
    tags = {
      "Environment" = var.environmnet_name
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}


####################################################
######APP instance auto scaling group ##############

resource "aws_autoscaling_group" "app_auto_scaling_group" {
  name_prefix = "ASG-${var.environmnet_name}"
  launch_template {
    id      = aws_launch_template.app_launch_template.id
    version = "$Latest"
  }
  max_size         = 10
  min_size         = 1
  desired_capacity = 1

  vpc_zone_identifier = [
    element(module.vpc.private_subnets, 0),
    element(module.vpc.private_subnets, 1),
    element(module.vpc.private_subnets, 2)
  ]

  default_cooldown          = 180
  health_check_grace_period = 180
  health_check_type         = "EC2"

  target_group_arns = [
    aws_lb_target_group.app_lb_target_group.arn,
  ]

  termination_policies = [
    "OldestLaunchConfiguration",
  ]

  tags = concat(
    list(map("key", "Name", "value", "ASG-${var.environmnet_name}", "propagate_at_launch", true)),

  )

  lifecycle {
    create_before_destroy = true
  }
}